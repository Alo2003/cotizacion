const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener("click", function(){

   //obtener los valores de los inputs text

   let valorAuto = document.getElementById('valorAuto').value;
   let pIncial = document.getElementById('porPagoIncial').value;
   let plazos = document.getElementById('plazos').value;

   //hacer calculos
   let pagoIncial = valorAuto * (pIncial/100);
   let totalFin = valorAuto-pagoIncial;
   let pagoMensual = totalFin/plazos;


   //mopstrar los resultados

   document.getElementById('pagoIncial').value = pagoIncial;
   document.getElementById('totalFin').value = totalFin;
   document.getElementById('pagoMensual').value = pagoMensual;

})
